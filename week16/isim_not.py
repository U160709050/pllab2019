import time

f = open("grades.txt", "w") #dosya oluşturulur

f.write("Ali AA\n")
f.write("Veli AA\n")
f.write("Bilal BB\n")
f.write("Oya BB\n")
f.write("Kaya BA\n")
f.write("Mercan CB\n")
f.write("Suphi AB\n")
f.write("Şeyma DD\n")
f.write("Serdar FF\n")
f.write("Nazmiye CD\n")
f.write("Soner BB\n")

f.close()


f = open("grades.txt", "r") #yazılan dosyayı okuma modunda açar

file_lines = f.readlines() # tüm satırları bir listeye attık

grade_info = {} # python hash veri yapısı oluşturduk



for f_line in file_lines:
    f_line = f_line.strip()  # \n i sildim.
    print(f_line)
    info = f_line.split() # boşluklar ayrıldıldı ikili ifadelere geldi

    name = info[0]
    grade = info[1]

    if grade not in grade_info:
        grade_info[grade] = {name: f_line} # bu nota ait ilk kayıt ekleniyip dict oluşturdum
    else:
        grade_info[grade][name] = f_line # sonraki gelen sisimler yukarda oluşturduğum dict e ekledim

print()

print("original: ", grade_info)

start = time.time()
sorted_grade_info = dict(sorted(grade_info.items(), reverse=True)) # #notu değer olarak değil de alfabetik olarak artan sırada dizdim

print("sorted  : ", sorted_grade_info)
end = time.time()

time_elapsed = end - start

print("sorted by grades in ascending order in {} seconds".format(time_elapsed))

print()
# silme işlemi
start = time.time()

for key, second_dict in sorted_grade_info.items(): # key:not, second_dict:bu nota ait kayıtlar
    #print(key, second_dict)
    for k, v in second_dict.items():
        #print(k,v)

        if k == "Ali":
            break # ali ye rastlarsa iç for dan çıkıyor ve aşağıdaki break ile dış fordan da çıkıyor
    else:
        continue  # ali değilse bir sonraki ismi kontrol eder
    break

# elde ettiği key ve k yi silme işlemini gerçekleştirdik
# key:AA, Ali nin notu,k:"Ali" ismi

del sorted_grade_info[key][k]
end = time.time()

time_elapsed = end - start
print("updated : ", sorted_grade_info)
print("Ali is deleted in {} seconds".format(time_elapsed))


